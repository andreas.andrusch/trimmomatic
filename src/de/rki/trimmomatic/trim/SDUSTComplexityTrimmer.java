// based on Aleksandr Morgulis, E. Michael Gertz, Alejandro A. Schäffer, and Richa Agarwala.
// Journal of Computational Biology.Jun 2006.
// http://doi.org/10.1089/cmb.2006.13.1028

package de.rki.trimmomatic.trim;

import org.usadellab.trimmomatic.fastq.FastqRecord;
import org.usadellab.trimmomatic.trim.AbstractSingleRecordTrimmer;

import java.util.HashMap;

public class SDUSTComplexityTrimmer extends AbstractSingleRecordTrimmer {
    private int windowSize;
    private double maxDust;
    private int kmerSize;

    public SDUSTComplexityTrimmer() {
        this.windowSize = 64;
        this.maxDust = 2.0;
        this.kmerSize = 3;
    }

    public SDUSTComplexityTrimmer(String args) {
        String arg[] = args.split(":");
        this.windowSize = Integer.parseInt(arg[0]);
        this.maxDust = Double.parseDouble(arg[1]);
        this.kmerSize = Integer.parseInt(arg[2]);
    }

    @Override
    public FastqRecord processRecord(FastqRecord in) {
        if (in.getSequence().length() <= kmerSize) {
            return in;
        }

        // calculate dust score over windows and mark regions as masked
        boolean[] mask = getMaskArray(in);
        // get longest unmasked continuous stretch
        int[] startAndLength = getStartAndLengthOfLongestContinuousStretch(mask);

        if (startAndLength[1] == 0) {
            return null;
        }
        return new FastqRecord(in, startAndLength[0], startAndLength[1]);
    }

    private boolean[] getMaskArray(FastqRecord in) {
        boolean[] mask = new boolean[in.getSequence().length()];
        int maxPos = in.getSequence().length()-windowSize;

        for (int pos = 0; pos <= maxPos; pos += kmerSize) {
            double dustScore = getDustScore(in.getSequence().substring(pos, pos+windowSize));
            if (dustScore > maxDust) {
                for (int maskPos = pos; maskPos < pos + windowSize; maskPos++) {
                    mask[maskPos] = true;
                }
            }
        }
        return mask;
    }

    private static int[] getStartAndLengthOfLongestContinuousStretch(boolean[] mask) {
        // get indices of longest continuous false (unmasked) sequence in the array
        int start = 0;
        int length = 0;
        boolean counting = false;
        int[] result = new int[2]; // start at index 0 and length at index 1

        for (int index = 0; index<mask.length; index++) {
            boolean positionMasked = mask[index];
            if (!positionMasked && counting) {
                length++;
            } else if (!positionMasked && !counting) {
                counting = true;
                start = index;
                length = 1;
            } else if (positionMasked && counting) {
                counting = false;
                result = updateStartAndLengthIfNecessary(start, length, result);
            }
        }
        return updateStartAndLengthIfNecessary(start, length, result);
    }

    private static int[] updateStartAndLengthIfNecessary(int start, int length, int[] result) {
        if (length > result[1]) {
            result[0] = start;
            result[1] = length;
        }
        return result;
    }

    private double getDustScore(String sequence) {
        // formula is sum(count(kmer)*(count(kmer)-1)/2)/(l-1)
        if (sequence.length() == kmerSize) {
            return 0.0;  // as defined in the paper
        }

        Integer[] kmerCounts = getKmerCounts(sequence);
        int sum = 0;
        for (int kmerCount: kmerCounts) {
            sum += kmerCount*(kmerCount-1)/2;
        }
        int l = sequence.length()-kmerSize+1;  // in the paper: l==n-2 with n==sequence length and 2==kmerSize-1
        return (double) sum/((double) l-1.0);
    }

    private Integer[] getKmerCounts(String sequence) {
        HashMap<String, Integer> kmerCounterMap = new HashMap<String, Integer>();

        int maxPos = sequence.length()-kmerSize;
        for (int pos = 0; pos <= maxPos; pos++) {
            String kmer = sequence.substring(pos, pos+kmerSize);
            if (!kmerCounterMap.containsKey(kmer)) {
                kmerCounterMap.put(kmer, 1);
            }
            else {
                kmerCounterMap.put(kmer, kmerCounterMap.get(kmer)+1);
            }
        }

        return kmerCounterMap.values().toArray(new Integer[0]);
    }
}
