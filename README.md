#### Original release and documentation at:

http://www.usadellab.org/cms/?page=trimmomatic

---

#### New features in the RKI fork:

New trim mode SDUST (based on Morgulis A. et al. (2006)
A fast and symmetric DUST implementation to mask low-complexity DNA sequences.
J. Comput. Biol., 13, 1028–1040.)

The new trimmer is called SDUST and has 3 parameters with defaults from the
paper:
1. windowSize, an integer with default 64, giving the size of the sliding window
2. maxDust, a double with default 2.0, giving the maximum score a window may
have before being masked
3. kmerSize, an integer with default 3, giving the size of the kmers to use
during scoring, altering this should go together with raising maxDust
accordingly

Usage example:

    java -jar trimmomatic-0.38.2.jar SE in.fastq out.fastq SDUST:64:2.0:3
